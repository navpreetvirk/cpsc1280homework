# FILE TO BE PROCESSED
file=$1

# LIST OF CATEGORIES
category_list=($(cat $file | cut -d "," -f 2 | tail -n +2 | sort | uniq))

# NUMBER OF CATEGORIES
category_count=${#category_list[*]}

# EXTRACT FIRST HEADING
first_heading=$(head -n 1 $file | sed -n 's/,.*//p')

# NUMBER OF DATA ROWS EXCLUDING THE HEADING ROW
data_row_count=$(expr $(wc -l < data.csv) - 1)

# PRINT HEADING ROW
    # PRINT FIRST HEADING
    echo -n $first_heading

    # PRINT CATEGORIES
    for index in $(seq 0 $(expr $category_count - 1))
    do
        echo -n ",${category_list[$index]}"
    done

    # NEW LINE AFTER HEADING ROW
    echo

# PRINT VALUES AND CATEGORIES
    echo $data_row_count