file=$1

column1=`cut -d ',' -f1 $file | head -n1`
categories=$(cut -d ',' -f2 $file | tail -n +2 | sort | uniq)
categoryCount=$(cut -d ',' -f2 $file | tail -n +2 | sort | uniq | wc -l)
categoryList=$(cut -d ',' -f2 $file | tail -n +2)
rowCount=$(expr `wc -l < $file` - 1)
valueList=$(cut -d ',' -f1 $file | tail -n +2)

printf "$column1,"

for count in $(seq 1 $categoryCount)
do
    printf `echo $categories | cut -d ' ' -f $count`
    if [ $count != $categoryCount ]
    then
        printf ','
    fi
done

printf '\n'

for count in $(seq 1 $rowCount)
do
    printf "`echo $valueList | cut -d ' ' -f $count`,"
    
    for count2 in $(seq 1 $categoryCount)
    do
        if [ `echo $categoryList | cut -d ' ' -f $count` != `echo $categories | cut -d ' ' -f $count2` ]
        then
            printf '0'
            if [ $count2 != $categoryCount ]
            then
                printf ','
            fi
        else
            printf '1'
            if [ $count2 != $categoryCount ]
            then
                printf ','
            fi
        fi
    done
    
    printf '\n'
done