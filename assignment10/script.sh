#!/bin/bash

function ipToDecimal() {
    ipArray=($(echo $1 | tr "." " "))
    
    octet1=$(expr ${ipArray[0]} \* 16777216)
    octet2=$(expr ${ipArray[1]} \* 65536)
    octet3=$(expr ${ipArray[2]} \* 256)
    octet4=${ipArray[3]}
    
    echo $octet1 + $octet2 + $octet3 + $octet4 | bc
}

function decimalToIp() {
    number=$1
    
    octet1=$(echo $number / 16777216 | bc)
    let "number -= $octet1 * 16777216"
    octet2=$(echo $number / 65536 | bc)
    let "number -= $octet2 * 65536"
    octet3=$(echo $number / 256 | bc)
    let "number -= $octet3 * 256"
    octet4=$(echo $number / 1 | bc)
    
    echo $octet1.$octet2.$octet3.$octet4
}

ip1Decimal=$(ipToDecimal $1)
ip2Decimal=$(ipToDecimal $2)

if [ $ip2Decimal \< $ip1Decimal ]
then
    echo -e "\e[31mInvalid IP range\e[0m"
    exit 1
fi

echo "Server Address,Reachable,Average Time,First TTL,Failed Ping(s),Successf\
ul Ping(s),Initiate Time" > report.csv

for ((i=$ip1Decimal; i<=$ip2Decimal; i++))
do
    initTime=$(date +%r)
    ping -c 10 -W 2 $(decimalToIp $i) > temp
    
    if [ $? == 1 ]
    then
        echo "$(decimalToIp $i),No,-,-,-,-,-"
    else
        serverName=$(grep "ping statistics" temp | \
        sed -n "s/^.*- \(.*\) p.*$/\1/p")
        
        average=$(grep "rtt min/avg/max/mdev = " temp | \
        sed -n "s/^.*= .*\/\(.*\)\/.*\/.*$/\1/p")
        
        ttl1=$(grep "ttl=" temp | head -n 1 | \
        sed -n "s/^.*ttl=\(.*\) time.*$/\1/p")
        
        successful=$(grep "packets transmitted," temp | \
        sed -n "s/^.*d, \(.*\) r.*$/\1/p")
        
        failed=$(((10 - $successful)))
        
        echo "$serverName,Yes,$average,$ttl1,$failed,$successful,$initTime"
    fi
done >> report.csv

rm temp