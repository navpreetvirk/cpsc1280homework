# File to be processed
file=$1

# Count number of lines starting with string '"Ciutat Vella",'
number=$(grep -c '^"Ciutat Vella",' immigrants_emigrants_by_destination2.csv)

# Last line of 'Ciutat Vella' section
end=$(grep -n '^"Ciutat Vella",' $file | sed -n '$p' | sed -n 's/:.*//p')

# Appends total row to end of 'Ciutat Vella' section
sed -n -e "${end} a \"Total Number of People\",${number}," -e '1,$ p' $file # > Output