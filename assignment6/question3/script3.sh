# File to be processed
file=$1
# Bird name or pattern
bird="$2"

################################################################################

# Line number containing the bird name or pattern
line=$(sed -n -e "/^ *$bird /=" $file)

################################################################################

# Print the bird name block
sed -n "$line,\$ p" $file | sed '/^[[:space:]]*$/,$ d'

# Empty line
echo

# Print the definition block
sed -n "$line,\$ p" $file | sed -n '/^DESCRIPTION: /,$ p' \
| sed '/^[[:space:]]*$/,$ d'