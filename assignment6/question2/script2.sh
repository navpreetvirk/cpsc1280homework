# File to be processed
file=$1

# Find all lines starting with one or more upper case letters followed by a
# comma ',' a space ' ' and string 'adj.' followed by another space ' ', and
# removes comma ',' and all following characters.

grep '^[[:upper:]]\+, adj\. ' $file | sed -n 's/,.*//p' # > Output