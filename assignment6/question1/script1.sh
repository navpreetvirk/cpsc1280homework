# Replace first and last occurances of commas ',' with strings '_START_' and
# '_END_' respectively and replace all characters in between including '_START_'
# and '_END_' strings with a comma ',' and exports the result to file 'Output'.

sed 's/,/_START_/2; s/,/_END_/10;
s/_START_.*_END_/,/' $1 > Output