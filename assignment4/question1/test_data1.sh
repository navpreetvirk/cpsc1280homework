# create test1
if [ -d test1 ]
then rm -r test1
fi
mkdir test1
cd test1

# create test dir, file and links
mkdir -p script1/{cookie,cake,biscuit/cheesecake}
echo "test file" > script1/file1.txt
echo "test file" > script1/file2.txt
ln script1/file2.txt script1/cookie/cookie
ln script1/file2.txt script1/cookie/choco
ln script1/file2.txt script1/cookie/oats
ln script1/file2.txt script1/cookie/granola
echo "test file" > script1/cookie/cake
echo "test file" > script1/cookie/biscuit
ln script1/cookie/cake script1/biscuit/oreo
ln script1/cookie/cake script1/biscuit/redbull
echo "test file" > script1/biscuit/kitkat
ln script1/cookie/biscuit script1/biscuit/cheesecake/toffee
ln script1/cookie/biscuit script1/biscuit/cheesecake/gum
ln script1/cookie/biscuit script1/biscuit/cheesecake/candy
ln script1/cookie/biscuit script1/biscuit/cheesecake/drink
ln script1/cookie/biscuit script1/biscuit/cheesecake/coke
echo "test file" > script1/biscuit/cheesecake/caramel
ln script1/biscuit/kitkat script1/biscuit/cheesecake/icecream
echo "test file" > script1/cake/concrete
echo "test file" > script1/cake/shoes
echo "test file" > script1/cake/lake