# create temp dir
if [ -d temp ]
then rm -r temp
fi
mkdir temp

# find all files and print file inode, and # of hard links
find $1 -type f -printf "%i,%n\n" | sort -k 2 -r -t "," | uniq | head -n 10 > temp/f1

# print to stdout
# header
printf "\t\tINODE\tLINKS\n\t\t-----\t-----\n" > output
cut -d "," -f 1,1 temp/f1 > temp/f2
cut -d "," -f 2 temp/f1 > temp/f3

# print inode and link count with line numbers
paste temp/f2 temp/f3 | cat -n >> output

# references to first inode
printf "\nReferences to $(cut -d "," -f 1,1 temp/f1 | head -n 1):\t" >> output
printf "\t\n`find $1 -inum $(cut -d "," -f 1,1 temp/f1 | head -n 1)`\n" >> output

# remove temp dir
rm -r temp