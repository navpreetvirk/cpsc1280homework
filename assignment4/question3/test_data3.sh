# remove test directories if they already exist
if [ -d DIR1 ]
then rm -r DIR1
fi
if [ -d DIR2 ]
then rm -r DIR2
fi

# create directory structure
mkdir DIR1 DIR1/L1a DIR1/L1a/L2a DIR1/L1a/L2b
mkdir DIR2 DIR2/L1a DIR2/L1a/L2a DIR2/L1a/L2c

# create files
touch DIR1/L1a/L2a/test1 DIR1/L1a/L2b/test1 DIR2/L1a/L2a/test1 DIR2/L1a/L2c/test1