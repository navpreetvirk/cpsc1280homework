# temp directory
if [ -d temp ]
then rm -r temp
fi
mkdir temp

# dir 1 contents
cd $1
find . -type f | sort > ../temp/dir1contents

# dir 2 contents
cd ../$2
find . -type f | sort > ../temp/dir2contents

# unique and common file extraction to three files respectively
cd ../
comm -23 temp/dir1contents temp/dir2contents > $3
comm -13 temp/dir1contents temp/dir2contents > $4
comm -12 temp/dir1contents temp/dir2contents > $5

#remove temp
rm -r temp