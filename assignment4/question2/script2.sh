# temp directory
if [ -d temp ]
then rm -r temp
fi

mkdir temp

# column extraction
cut -f 7 -d "," $1 | tr -d "\r" > temp/column7
cut -f 6 -d "," $1 > temp/column6
cut -f 5 -d "," $1 > temp/column5
cut -f 3 -d "," $1 > temp/column3
cut -f 4 -d "," $1 > temp/column4

#column reordering
paste -d "," temp/column7 temp/column6 temp/column5 temp/column3 temp/column4 > temp/reordered

# final output to output file
printf "Total number of Bus Stops: `tail -n +2 temp/column5 | uniq -u | wc -l`\n\n" > output
head -n +1 temp/reordered >> output
tail -n +2 temp/reordered | sort -k 1.2,1.2 temp/reordered >> output

# remove temp
rm -r temp