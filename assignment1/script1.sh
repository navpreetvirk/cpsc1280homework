# Create the following directory tree in the current directory
#
#                                  script2
#            _________________________|_________________________
#           |                   |               |               |
#        Pumpkin              Diakon          Carrot        Parsenip
#     ______|______         ____|____        ___|___         ___|___
#    |             |       |         |      |       |       |       |
# kabocha   WinterMelon   Pickle   Oden   Orange   Oden   Chips   Soup
#
mkdir -p script1/{Pumpkin/{kabocha,WinterMelon},Diakon/{Pickle,Oden},Carrot/{O\
range,Oden},Parsenip/{Chips,Soup}}

# Change permissions
#
# Read only for Pumpkin and Parsenip directory
chmod 444 script1/Pumpkin script1/Parsenip
# Accesible only by owner for Diakon directory
chmod 700 script1/Diakon
# Not accessible by anyone for Carrot directory
chmod 000 script1/Carrot

# Copy pg58.txt file to Pickle directory
cp ../Datasets/pg58.txt script1/Diakon/Pickle/

# Grant full access to owner before performing copy operation on Carrot
# directory
chmod 700 script1/Carrot
# Copy pg972.txt file to Carrot and Oden directories respectively
cp ../Datasets/pg972.txt script1/Carrot/
cp ../Datasets/pg972.txt script1/Carrot/Oden/
# Restore permissions after copy operation
chmod 000 script1/Carrot

# Grant full access to owner before performing copy operation on Pumpkin and
# Parsenip directories respectively
chmod 744 script1/Pumpkin script1/Parsenip
# Copy heart.csv file to Pumpkin and Chips directories respectively
cp ../Datasets/heart.csv script1/Pumpkin/
cp ../Datasets/heart.csv script1/Parsenip/Chips/
# Restore permissions after copy operation
chmod 444 script1/Pumpkin script1/Parsenip