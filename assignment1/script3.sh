# Writes my name (Navpreet Virk) to result.txt
echo Navpreet Virk > result.txt

# Append listing of the current directory to result.txt
ls -l >> result.txt

# Append current directory to result.txt
pwd >> result.txt

# Append my username (navpreetvirk) to result.txt
echo navpreetvirk >> result.txt