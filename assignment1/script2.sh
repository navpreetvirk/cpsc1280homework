# Create the following directory tree
#
#                      script2
#         ________________|_________________
#        |              |                   |
#     antelope        bears                cat
#     ___|___         __|___            ____|____
#    |       |       |      |          |         |
# sitting   bear   cubs   spray      large     small
#            |              |          |         |
#          fight        dangerous   Persian   unknown
#
mkdir -p script2/{antelope/{sitting,bear/fight},bears/{cubs,spray/dangerous},c\
at/{large/Persian,small/unknown}}