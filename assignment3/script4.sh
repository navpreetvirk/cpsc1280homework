# Prints HTML doctype declaration and head code
printf "<!DOCTYPE html>
<html lang=\"en\">
<head>
\t<meta charset=\"UTF-8\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
\t<title>Document</title>
</head>
<body>\n"
# Prints filename in h2 tag and first 5 lines of file (as shown in assignment picture) in pre tag, also prints word count
# Added extra \n's for easy readability
find . -name "$1" -type f -printf "\n<h2>Filename: %p</h2>\n<pre>\n" -exec sed -n 1,5p {} \; -exec printf "</pre>\n<br>\nWord Count " \; -exec sh -c "wc -w < {}" \;
# Prints end of the document
printf "\n</body>\n</html>\n";