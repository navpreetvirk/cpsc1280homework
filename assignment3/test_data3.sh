# Remove test3 directory if it exists
if [ -d test3 ]
then rm -r test3
fi
# Create test3 directory and its sub(sub...)directories and files
mkdir -p test3/{apple,adobe/{free,paid},microsoft/office,ubisoft/{farcry,thecrew}}
touch test3/adobe/free/reader.file test3/adobe/paid/photoshop.file \
test3/apple/garageband.file \
test3/microsoft/office/access.file test3/microsoft/office/word.file test3/microsoft/visio.file \
test3/ubisoft/farcry/farcry5.file test3/ubisoft/thecrew/thecrew2.file