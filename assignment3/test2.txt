Emily Brontë’s novel of passion and cruelty, published in 1847, was the only
novel she ever wrote and one of which many, including her sister Charlotte,
disapproved, regarding it as fundamentally immoral, especially in the creation
of the central character, the brutal Heathcliff. However, viewed at a distance
of some 150 years, the novel can be seen for what it truly is, a work of flawed
genius which continues to attract strongly despite its age. Emily set what was
to be her sole novel in and around her beloved moors creating, in Cathy, a
character as wilful as herself. However the reader acquainted but not familiar
with the narrative, is often surprised by how little actual description of the
natural environment is extant within its pages though ‘metaphors drawn from
nature provide much of the book’s descriptive language’. Simply expressed, it
is the author’s own vicarious resonance with the land, expressed via her
frequent use of what Ruskin termed ‘pathetic fallacy’ that gives the intensity
of the connective between the central protagonists and the land in which they
are imbedded, even beyond life itself. The plot concerns the family of the
Earnshaws, owners of the eponymous ‘Wuthering Heights’, where the surly urchin,
Heathcliff, is brought by the father of the household who has found him
abandoned in Liverpool, and who describes him ‘as dark almost as if it came
from the devil’ for ‘when Mr. Earnshaw first brings the child home, the child
is an “it” not a “he”’. From the first, he is Cathy, the daughter’s favourite,
as he is her father’s, and the thorn in the flesh of the heir, Hindley. Both
boys, indeed, loathe each other with a passion partly born of ‘sibling rivalry’,
even though they are not blood relatives (at least such is not openly stated
even if critics have inferred more than an act of philanthropy in Mr. Earnshaw’s
rescuing the boy and his wife’s attendant animosity). When Earnshaw dies,
Hindley wastes no time in correcting the usurpation from which he believes he
has suffered by consigning Heathcliff to the level of a servant. Meanwhile,
Cathy and Heathcliff have formed a bond which nothing will ever break, even
Cathy’s marriage to the wealthy Edgar Linton.