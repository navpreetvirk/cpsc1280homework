# Heading for convinience
echo "Deleted Files:" > $3
# Delete files named $2
find $1 -name "$2" -type f -delete -printf "\t%p\n" >> $3
# Heading for convinience
printf "\nDeleted Directories:" >> $3
# Delete directories not named $2
find $1 -mindepth 1 ! -name "$2" -type d -delete -printf "\n\t%p" >> $3