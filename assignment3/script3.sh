# Remove $2 directory if it exists
if [ -d $2 ]
then rm -r $2
fi
mkdir $2
# Replicates directories structure from $1 to $2 directory and create file hard
# links at same relevant paths
cd $1
find . -type d -exec mkdir -p ../$2/{} \;
find . -type f -exec ln {} ../$2/{}.bak \;
# Prints first row to tmp (temporary file)
printf "`date`\tsource: $1\tdestination: $2\n" >> ../tmp
# Prints inodes and file names of both directory files to tmp
find . -type f -printf '%i\t%p\n' -exec find ../$2 -type f -samefile {} -printf '%i\t%p\n' \; >> ../tmp
# cat read tmp file and number the lines, and forward the output to $3 file
cat -n ../tmp >> ../$3
#Remove tmp file
rm ../tmp