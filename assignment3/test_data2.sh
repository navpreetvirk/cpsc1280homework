# Remove test2 directory if it exists
if [ -d test2 ]
then rm -r test2
fi
# Create test2 directory and its sub directories and files
mkdir test2
cd test2
mkdir unix1 unix2 unix3 windows1 windows2 windows3
touch unix1.file unix2.file unix3.file windows1.file windows2.file windows3.file