The history of Unix begins at AT&T Bell Labs in the late 1960s with a small team of
programmers looking to write a multi-tasking, multi-user operating system for the
PDP-7. Two of the most notable members of this team at the Bell Labs research facility
were Ken Thompson and Dennis Ritchie. While many of Unix's concepts were
derivative of its predecessor (Multics), the Unix team's decision early in the 1970s to
rewrite this small operating system in the C language is what separated Unix from all
others. At the time, operating systems were rarely, if ever, portable. Instead, by nature
of their design and low-level source language, operating systems were tightly linked to
the hardware platform for which they had been authored. By refactoring Unix on the C
programming language, Unix could now be ported to many hardware architectures.
