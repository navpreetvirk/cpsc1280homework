#!/bin/bash

word_list=$1
document=$2
word_pattern=$(cat $1 | tr "\n" "|" | sed -n "s/|$// p")

occurance=0

while read document_line
do
    while read word_list_line
    do
        ((occurance+=$(echo $document_line | grep -i -c "$word_list_line")))
    done < $1
    
    if [ $occurance == 1 ]
    then
        echo $document_line | sed -nE "s/$word_pattern/----/I p"
    elif [ $occurance -gt 1 ]
    then
        echo $document_line | sed -n "s/./-/gIp"
    else
        echo $document_line
    fi
    
    occurance=0
done < $2